let rootRef = firebase.database().ref().child('Articles');

rootRef.on('child_added', snap => {
    let title = snap.child('title').val();
    let url = snap.child('url').val();
    
    let singleArticle = document.createElement('ARTICLE');
    singleArticle.classList.add('single-article');
    
    let titleNode = document.createElement('H2');
    
    
    let linkNode = document.createElement('A');
    let linkOnCAtt = document.createAttribute('onclick');
    linkOnCAtt.value = 'openArticle()';
    let linkTextNode = document.createTextNode(title);
    
    linkNode.setAttributeNode(linkOnCAtt);
    linkNode.appendChild(linkTextNode);
    titleNode.appendChild(linkNode);
    
    
    let imgNode = document.createElement('IMG');
    let imgClsAtt = document.createAttribute('class');
    imgClsAtt.value = 'single-article-img';
    let imgSrcAtt = document.createAttribute('src');
    imgSrcAtt.value = url;
    
    imgNode.setAttributeNode(imgSrcAtt);
    imgNode.setAttributeNode(imgClsAtt);
    
    singleArticle.appendChild(titleNode);
    singleArticle.appendChild(imgNode);
    
    document.getElementsByClassName('article-list')[0].appendChild(singleArticle);
});

function openArticle() {
    let lsKey = 'ArticleToOpen';
    let lsVal = '-LPep1kc9yDatC9w9qeZ';

    localStorage.setItem(lsKey, lsVal);
    window.open('article.html');
   
}
