let rootRef = firebase.database().ref();

rootRef.on('child_added', snap => {
        let articleToOpen = localStorage.getItem('ArticleToOpen');
        
        if(articleToOpen){
            let imgUrl = snap.child(articleToOpen).child('url').val();
            let title = snap.child(articleToOpen).child('title').val();
            let desc = snap.child(articleToOpen).child('desc').val();
            let text = snap.child(articleToOpen).child('text').val();
   
            document.getElementsByClassName('article-info-image')[0].setAttribute('src',imgUrl);
            document.getElementsByClassName('article-info-title')[0].innerHTML = title;
            document.getElementsByClassName('article-info-description')[0].innerHTML = desc;
            document.getElementsByClassName('article-text')[0].innerHTML = text;
       
        }else {
        alert('You must choose article from Home to display it on this page');
     
        }
});